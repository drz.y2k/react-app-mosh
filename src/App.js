import React, { Component } from 'react';
import NavBar from './components/navbar'
import Counter from './components/counter'
import Counters from './components/counters'

class App extends React.Component {

  state = {
    counters: [
      { id: 1, value: 4 },
      { id: 2, value: 0 },
      { id: 3, value: 0 },
      { id: 4, value: 0 },
    ],
  };

  constructor(props) {
    super(props);
    console.log('constructor');
    //we can initialize the state here, withtouh doing in the state object
    // this.state=this.props.something;
  }

  componentDidMount(){
    console.log('app mounted');
    //usually to make ajax calls
  }


  handleDelete = (counterId) => {
    // console.log('event handler called',counterId);
    const counters = this.state.counters.filter((c) => c.id != counterId);
    this.setState({ counters });
  };

  handleReset = () => {
    const counters = this.state.counters.map((c) => {
      c.value = 0;
      return c;
    });
    this.setState({ counters });
  };

  handleIncrement = (counter) => {
    // console.log(counter);
    const counters = [...this.state.counters];
    const index = counters.indexOf(counter);
    counters[index] = { ...counter };
    counters[index].value++;
    this.setState({ counters });
  };

  render() {
    console.log('app rendered');
    return (
      <React.Fragment>
        <NavBar totalCounters={this.state.counters.filter(c => c.value > 0).length} />
        <main className="container">
          <Counters onReset={this.handleReset} onIncrement={this.handleIncrement} onDelete={this.handleDelete} counters={this.state.counters} />
        </main>
      </React.Fragment>
    )
  }
};

export default App;
