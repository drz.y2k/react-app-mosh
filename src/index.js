import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
// import Counter from './components/counter'
// import Counters from './components/counters'
import { createRoot } from 'react-dom/client';
import App from './App'


createRoot(document.getElementById('root')).render(<App/>);
