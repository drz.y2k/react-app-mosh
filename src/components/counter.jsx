import React, { Component } from 'react';

class Counter extends Component {
	// state = {
	// 	value: this.props.counter.value,
	// 	tags: ['tag1', 'tag2', 'tag3'],
	// };

	// constructor(){
	//   super();
	//   this.handleIncrement=this.handleIncrement.bind(this);
	// }

	// handleIncrement() {
	// handleIncrement = (product) => {
	// 	// console.log('increment clicked', this.state);
	// 	this.setState({ value: this.state.value + 1 });
	// };

  componentDidUpdate(prevProps,prevState){
    console.log('prevp',prevProps);
    console.log('prevs',prevState);
    if(prevProps.counter.value!=this.props.counter.value){
      //get new data
      console.log('change');
    }
  }

  componentWillUnmount(){
    console.log('counter unmount');
  }



	renderTags() {
		// if (this.state.tags.length === 0) return <p>There are no tags!</p>;
		// else
    console.log('counter rendered');
			return (
				<div>
					<h1
						style={{ fontSize: 30 }}
						className={this.getBadgeClasses()}
					>
						{this.formatCount()}
					</h1>
					<button
						onClick={() => this.props.onIncrement(this.props.counter)}
						className="btn btn-secondary btn-sm"
					>
						Increment
					</button>
					{/* <ul>
						{this.state.tags.map((tag) => (
							<li key={tag}>{tag}</li>
						))}
					</ul> */}
					<button
						onClick={()=>this.props.onDelete(this.props.counter.id)}
						className="btn btn-danger btn-sm m-2"
					>
						Delete
					</button>
				</div>
			);
	}

	render() {
		return (
			<React.Fragment>
				{this.props.children}
				{/* {this.state.tags.length === 0 && 'Please create a new tag!'} */}
				{this.renderTags()}
			</React.Fragment>
		);
	}

	getBadgeClasses() {
		let classes = 'badge m-2 text-bg-';
		classes += this.props.counter.value === 0 ? 'warning' : 'primary';
		return classes;
	}

	formatCount() {
		const { value: count } = this.props.counter;
		return count === 0 ? 'Zero' : count;
	}
}

export default Counter;

