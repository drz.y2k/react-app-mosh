import React, { Component } from 'react';
import Counter from './counter';

class Counters extends Component {



	render() {
    console.log('counters rendered');
    const{onReset,counters,onDelete,onIncrement}=this.props;
		return (
			<div>
				<button
					onClick={onReset}
					className="btn btn-primary btn-sm m-2"
				>
					Reset
				</button>
				{counters.map((counter) => (
					<Counter
						key={counter.id}
						onDelete={onDelete}
						// value={counter.value}
						// id={counter.id}
						// selected
						counter={counter}
						onIncrement={onIncrement}
					>
						<h4>Title Children: #{counter.id}</h4>
					</Counter>
				))}
			</div>
		);
	}
}

export default Counters;

